'use client';

import { useRouter } from 'next/navigation';
import React, { useState } from 'react';

const TicketForm = () => {
  const router = useRouter();

  const handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;

    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch('/api/Tickets', {
      method: 'POST',
      body: JSON.stringify({ formData }),
      'content-type': 'application/json',
    });
    if (!res.ok) {
      throw new Error('Failed to create Ticket.');
    }
    router.refresh();
    router.push('/');
  };

  const startingTicketData = {
    title: '',
    description: '',
    category: 'Hardware Problem',
    priority: 1,
    progress: 0,
    status: 'not started',
  };

  const [formData, setFormData] = useState(startingTicketData);
  return (
    <div className='flex justify-center'>
      <form
        className='flex flex-col gap-3 w-1/2'
        method='post'
        onSubmit={handleSubmit}
      >
        <h3>Create Your Ticket</h3>
        <label htmlFor='title'>Title</label>
        <input
          id='title'
          name='title'
          type='text'
          onChange={handleChange}
          required={true}
          value={formData.title}
        />
        <label htmlFor='description'>Description</label>
        <textarea
          id='description'
          name='description'
          onChange={handleChange}
          required={true}
          value={formData.description}
          rows='5'
        />
        <label htmlFor='description'>Category</label>
        <select
          id='category'
          name='category'
          onChange={handleChange}
          required={true}
          value={formData.category}
          rows='5'
        >
          <option value='Hardware Problem'>Hardware Problem</option>
          <option value='Software Problem'>Software Problem</option>
          <option value='Project'>Project</option>
        </select>
        <label htmlFor='title'>Priority</label>
        <div className='flex gap-5'>
          <div>
            <input
              id='priority-1'
              name='priority'
              type='radio'
              onChange={handleChange}
              required={true}
              value={1}
              checked={formData.priority == 1}
            />
            <label>1</label>
          </div>
          <div>
            <input
              id='priority-2'
              name='priority'
              type='radio'
              onChange={handleChange}
              required={true}
              value={2}
              checked={formData.priority == 2}
            />
            <label>2</label>
          </div>
        </div>
        <label htmlFor='title'>Progress</label>
        <input
          id='progress'
          name='progress'
          type='range'
          onChange={handleChange}
          required={true}
          value={formData.progress}
          min='0'
          max='100'
        />
        <label htmlFor='description'>Status</label>
        <select
          id='status'
          name='status'
          onChange={handleChange}
          required={true}
          value={formData.status}
          rows='5'
        >
          <option value='not started'>Not Started</option>
          <option value='started'>Started</option>
          <option value='done'>Done</option>
        </select>
        <input type='submit' className='btn max-w-xs' value='Create Ticket' />
      </form>
    </div>
  );
};

export default TicketForm;
